#include "filehandler.hpp"
#include <iostream>
#include <fstream>

grid* readFile(std::string filename)
{
    int n;
    int sum = 0;
    int gridsize;
    std::vector<int> temp;
    std::ifstream inFile;
    inFile.open(filename);

    if (!inFile)
    {
        std::cerr << "Unable to open file " << filename;
        exit(-1);
    }
    while (inFile >> n)
    {
        if (sum == 0)
        {
            gridsize = n;
        }
        else
        {
            temp.push_back(n);
        }
        
        sum++;
    }


    grid* values = new grid (gridsize, std::vector<Node>(gridsize));

    int x = 0;
    int y = 0;

    //create grid from file values
    for (int i = 0; i < temp.size(); i++)
    {
        if (y>= gridsize)
        {
            x++;
            y = 0;
        }
        (*values)[x][y].colour = temp[i];
        (*values)[x][y].connected = false;
        y++;
    }

    inFile.close();

    return values;
    
}