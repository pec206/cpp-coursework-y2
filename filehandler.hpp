#ifndef FILEHANDLER
#define FILEHANDLER

#include <string>
#include "board.hpp"

grid* readFile(std::string filename);

#endif
