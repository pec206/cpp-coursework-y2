#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include "filehandler.hpp"
#include "board.hpp"

int main(int argc, char const *argv[])
{
    std::cout << "Welcome to Board Game! \nPlease enter f to read board from a file or r to use a randomly generated board:" << std::endl;
    std::string userIn;
    std::cin >> userIn;
    srand(time(NULL));
    int gridsize = 0;
    grid gridvalues;

    

    if (userIn == "f")
    {   
        std::string filename;
        std::cout << "Please enter file name:" << std::endl;
        std::cin >> filename;
        // readfile
        gridvalues = *readFile(filename);
        gridsize = gridvalues.size();
        
        if (gridsize < 3 || gridsize > 9)
        {
            std::cout << "Error: please enter a grid size between 3 and 9 inclusive" << std::endl;
            exit(-1);
        }

        //generate grid
        Board gameBoard(gridsize,gridvalues);
        Board testBoard(gridsize,gameBoard.values);
        
        gameBoard.printBoard();
        testBoard.findEstMinMoves();
        //play game
        gameBoard.play();

    }
    else if (userIn == "r")   
    {
        std::cout << "Please enter a grid size:" << std::endl;
        std::cin >> gridsize;
        if (gridsize < 3 || gridsize > 9)
        {
            std::cout << "Error: please enter a grid size between 3 and 9 inclusive" << std::endl;
            exit(-1);
        }

        Board gameBoard(gridsize);
        Board testBoard(gridsize,gameBoard.values);
        
        gameBoard.printBoard();
        testBoard.findEstMinMoves();
        //play game
        gameBoard.play();
    }
    else
    {
        std::cout << "Error: pLease only enter f or r" << std::endl;
        exit(-1);
    }

    


    return 0;
}


