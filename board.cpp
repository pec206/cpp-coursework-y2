#include "board.hpp"
#include <random>
#include <iostream>

//DESTRUCTOR?
//Board constructor
Board::Board(int gridsize) : size(gridsize)
{
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> distribution(1,6);

    grid v (gridsize, std::vector<Node>(gridsize));
    values = v;
    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {
            values[x][y].colour = distribution(generator);
            values[x][y].connected = false;
        }
    }
    values[0][0].connected = true;
}

Board::Board(int gridsize, grid newValues) : size(gridsize), values(newValues)
{
    values[0][0].connected = true;
}

Board::~Board()
{
    values.clear();
}


//Board methods
void Board::printBoard()
{
    std::cout << "Board:" << std::endl;
    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {   
            std::cout << values[x][y].colour << " ";
        }
        std::cout << '\n';
    }

}

bool Board::isConnected(int x, int y) 
//checks to see if values[x][y] is connectected to the pivot
{
    //check right
    int test = y+1;
    if (test < size)
    {
        if (values[x][test].connected)
        {
            return true;
        }
    }
    //check left
    test = y-1;
    if (test >= 0)
    {
        if (values[x][test].connected)
        {
            return true;
        }
    }
    //check down
    test = x+1;
    if (test < size)
    {
        if (values[test][y].connected)
        {
            return true;
        }
    }
    //check up
    test = x-1;
    if (test >=0)
    {
        if (values[test][y].connected)
        {
            return true;
        }
    }
    
    return false;
}

void Board::makeMove(int newColour)
{
    //makes the move, changing the colour of all the connected nodes and connecting any new nodes found
    
    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {   
            if (values[x][y].colour == newColour)
            {                
                if (isConnected(x,y)) 
                {
                    values[x][y].connected = true;
                }
            }
            else if (values[x][y].connected == true)
            {
                values[x][y].colour = newColour;
            }
        }
    }    
}

bool Board::checkWon()
{
    //checks if game is won
    int color = values [0][0].colour;
    int total = size*size;
    int count = 0;
    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {   
            if (values[x][y].colour == color)
            {
                count++;
            }
        }
    }
    if (count == total)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Board::countConnected() // counts no. of nodes connected
{
    int count = 0;

    for (int x = 0; x < size; x++)
    {
        for (int y = 0; y < size; y++)
        {
            if (values[x][y].connected == true)
            {
                count++;
            }
        }
    }
    return count;
}


void Board::findEstMinMoves()
{   
    //used a simple dijstra style algorithm which chooses each step of the
    //solution by whichever step takes it nearest to the final solution.
    //it does not see any steps ahead in the solution, making it less
    //efficient in smaller examples than some of its more complex counterparts.

    //initialise variables
    bool done = false;
    int total = size*size;
    int maxfilled; //how much of the board is filled
    int chosen = 0;
    int filled = 0; 
    int min = 0; //current min moves
    grid initialValues = values; //values saved for backstepping


    while (!done)
    {
        maxfilled = 0;
        //try all possible numbers
        //take whichever has the biggest impact on the board
        //repeat until board is filled and return the min count

        for (int i = 1; i < 7; i++)
        { 
            //make move
            makeMove(i);
            filled = countConnected();
            if (filled > maxfilled) /////////
            {
                //set max values
                maxfilled = filled;
                chosen = i;
            }
            //reset values
            values = initialValues;
        }
        
        //apply best move
        makeMove(chosen);
        //update values
        initialValues = values;
        //update min moves
        min++;
        done = checkWon();
    }
    std::cout << "Estimated minimum moves: " << min << std::endl;


}


void Board::play()
{
    int moves = 0;
    int newColour;
    bool done = false;

    while (!done)
    {
        
        std::cout << "Enter colour: ";
        if (!(std::cin >> newColour)) //catches letter inputs
        {
            std::cout << "Error: Please enter a number between 1 and 6" << std::endl;
            std::cin.clear();
            std::cin.ignore(10000,'\n');  
            continue;      
        }
        if (newColour < 1 || newColour > 6) //catches out of range inputs
        {
            std::cout << "Error: Please enter a number between 1 and 6" << std::endl;
            continue;
        }
        else
        {
            moves ++;
            std::cout << "Move number: " << moves << std::endl;
            makeMove(newColour);
            printBoard();
            
            done = checkWon();
        }
    }
    
    std::cout << "Game over in " << moves << " move(s)" << std::endl;

}
    


