#ifndef BOARDFILE
#define BOARDFILE

#include <vector>

//class definition, attributes and constructor and method declarations here

struct Node
{
    int colour;
    bool connected;
};

typedef std::vector<std::vector<Node>> grid;

class Board {
    public:
        int size;
        grid values;
        bool won;
    
    public:
        Board(int gridSize); //constructor
        Board(int gridsize, grid newValues); //constructor
        ~Board(); //destructor

        void printBoard();
        void makeMove(int newColour);
        bool checkWon();
        bool isConnected(int x, int y);
        int countConnected(); //counts number of connected values in grid
        void findEstMinMoves(); //finds estimated minimum number of moves
        void play();
        

};



#endif